SELECT * FROM company.customers;

explain customers;

create view myview as 
select *from customers
order by CustomerName;

select * from myview;

insert into myview
values(10000, "a", "a", "a", "a", "a", "a");

create view myview2 as 
select customers.*, orders.CustomerID c
from customers, orders
where customers.CustomerID = orders.CustomerID;


create index abc on customers(customerName);
explain customers;


